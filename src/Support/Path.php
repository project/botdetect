<?php

namespace Drupal\botdetect\Support;

/**
 * Helper class for handling botdetect library paths.
 */
final class Path {

  /**
   * Disable instance creation.
   */
  private function __construct() {}

  /**
   * Physical path of public directory.
   *
   * Which is located inside the captcha package.
   *
   * @return string
   *   The public directory path.
   */
  public static function getPublicDirPathInLibrary(): string {
    return self::getCaptchaLibPath() . '/botdetect/public/';
  }

  /**
   * Physical path of the captchal library package.
   */
  public static function getCaptchaLibPath(): string {
    return \Drupal::root() . '/../vendor/captcha-com/captcha/botdetect-captcha-lib';
  }

  /**
   * Physical path of simple-botdetect.php file.
   *
   * @return string
   *   The path to the simple-botdetect.php file.
   */
  public static function getSimpleBotDetectFilePath(): string {
    return __DIR__ . '/../Provider/simple-botdetect.php';
  }

  /**
   * Physical path to the botdetect.xml style configuration file.
   *
   * @return string
   *   The path to the botdetect.xml file.
   */
  public static function getConfigFilePath(): string {
    $settings = \Drupal::config('botdetect.settings');
    $fid = $settings->get('xml_file');
    if (empty($fid)) {
      $botdetect_path = \Drupal::service('extension.list.module')
        ->getPath('botdetect');
      $botdetect_real_path = \Drupal::service('file_system')
        ->realpath($botdetect_path);
      $lang = \Drupal::languageManager()->getCurrentLanguage()->getId();
      $file_path = $botdetect_real_path . '/assets/botdetect.' . $lang . '.xml';
      if (!file_exists($file_path)) {
        $file_path = $botdetect_real_path . '/assets/botdetect.xml';
      }
    }
    else {
      $file = \Drupal::service('entity_type.manager')
        ->getStorage('file')
        ->load($fid);
      $file_path = \Drupal::service('file_system')
        ->realpath($file->getFileUri());
    }
    if (empty($file_path) || !file_exists($file_path)) {
      $configDirPath = \Drupal::root() . '/../config/packages/';
      $file_path = $configDirPath . 'botdetect.xml';
    }
    return $file_path;
  }

}
