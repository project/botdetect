<?php

namespace Drupal\botdetect\Support;

use Captcha\Bundle\CaptchaBundle\Support\Exception\FileNotFoundException;

/**
 * Helper class to load the botdetect php library.
 */
class DrupalSimpleLibraryLoader {

  /**
   * Constructor.
   */
  public function __construct() {}

  /**
   * Load BotDetect CAPTCHA Library and override Captcha Library settings.
   */
  public function load() {
    // Load bd php library.
    self::loadBotDetectLibrary();
  }

  /**
   * Load BotDetect CAPTCHA Library.
   */
  protected function loadBotDetectLibrary() {
    $libPath = Path::getCaptchaLibPath();

    if (!self::isLibraryFound($libPath)) {
      throw new FileNotFoundException(sprintf('The BotDetect Captcha library could not be found in %s.', $libPath));
    }

    self::includeFile(Path::getSimpleBotDetectFilePath(), TRUE, $libPath);
  }

  /**
   * Check if the path to Captcha library is correct or not.
   */
  protected static function isLibraryFound($libPath) {
    return file_exists($libPath . '/botdetect/CaptchaIncludes.php');
  }

  /**
   * Include a file.
   *
   * @param string $filePath
   *   Path of file to include.
   * @param bool $once
   *   Set to TRUE if you want to include the file only once.
   * @param string $includeData
   *   Seems to be used to transmit $libPath to simple-botdetect.php.
   */
  protected static function includeFile($filePath, $once = FALSE, $includeData = NULL) {
    if (is_file($filePath)) {
      ($once) ? include_once $filePath : include $filePath;
    }
  }

}
