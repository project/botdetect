(function ($, Drupal, once) {

  Drupal.behaviors.botdetect = {
    attach: function (context, settings) {
      once('botdetect', '#botdetect-captcha', context).forEach(function (element) {
        var captcha = $('#botdetect-captcha').captcha({
          captchaEndpoint: settings.botdetect.captchaEndpoint,
        });
      });
    },
  };

})(jQuery, Drupal, once);
