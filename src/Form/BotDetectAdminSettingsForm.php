<?php

namespace Drupal\botdetect\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\FileInterface;
use Drupal\file\FileRepositoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure BotDetect settings for this site.
 */
class BotDetectAdminSettingsForm extends ConfigFormBase {

  const UPLOAD_LOCATION = 'private://botdetect/';

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The file repository service.
   *
   * @var \Drupal\file\FileRepositoryInterface
   */
  protected $fileRepository;

  /**
   * Constructs an BotDetect Settings form object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory for the form.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\file\FileRepositoryInterface $file_repository
   *   The file repository service.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    EntityTypeManagerInterface $entity_type_manager,
    FileRepositoryInterface $file_repository,
  ) {
    parent::__construct($config_factory);
    $this->entityTypeManager = $entity_type_manager;
    $this->fileRepository = $file_repository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('file.repository')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'botdetect_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['botdetect.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('botdetect.settings');

    $form['general'] = [
      '#type' => 'details',
      '#title' => $this->t('BotDetect settings'),
      '#open' => TRUE,
    ];

    $form['general']['captcha_style'] = [
      '#title' => $this->t('Captcha style name'),
      '#default_value' => $config->get('captcha_style'),
      '#description' => $this->t('Must be the <strong>exact same</strong> style name as the one specified in the <code>botdetect.xml</code> file.'),
      '#maxlength' => 40,
      '#required' => FALSE,
      '#type' => 'textfield',
    ];

    $xml_file = $config->get('xml_file');
    $form['general']['xml_file'] = [
      '#title' => $this->t('BotDetect XML file'),
      '#type' => 'managed_file',
      '#description' => $this->t('Upload your custom <code>botdetect.xml</code> file here.'),
      '#default_value' => $xml_file ? [$xml_file] : NULL,
      '#upload_location' => 'temporary://',
      '#upload_validators' => [
        'file_validate_extensions' => ['xml'],
      ],
      '#required' => FALSE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('botdetect.settings');

    $file = $this->processXmlFile($form_state);

    $config
      ->set('captcha_style', $form_state->getValue('captcha_style'))
      ->set('xml_file', $file?->id())
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Processes the uploaded botdetect.xml file.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @return \Drupal\file\FileInterface|null
   *   The processed file object, or NULL if no file was uploaded.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function processXmlFile(FormStateInterface $form_state): ?FileInterface {
    $file_value = $form_state->getValue('xml_file');
    if (empty($file_value) || !is_array($file_value)) {
      return NULL;
    }
    $fid = $file_value[0];
    if (empty($fid)) {
      return NULL;
    }
    /** @var \Drupal\file\FileInterface $file */
    $file = $this->entityTypeManager->getStorage('file')->load($fid);
    if (empty($file)) {
      return NULL;
    }
    // Rename newly uploaded file by moving or replacing it.
    $xmlTargetUri = self::UPLOAD_LOCATION . $this->getXmlFilename();
    if ($file->getFileUri() === $xmlTargetUri) {
      return $file;
    }
    else {
      $moved_file = $this->fileRepository->move(
        $file, $xmlTargetUri, FileSystemInterface::EXISTS_REPLACE);
      $moved_file->setFilename($this->getXmlFilename());
      // We don't want the botdetect.xml file to be garbage collected.
      $moved_file->setPermanent();
      $moved_file->save();
      return $moved_file;
    }
  }

  /**
   * Return the default botdetect.xml file name.
   *
   * @return string
   *   The BotDetect XML file name.
   */
  private function getXmlFilename(): string {
    return 'botdetect.xml';
  }

}
