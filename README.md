# BotDetect Captcha

Integration of the [BotDetect Captcha Generator](https://captcha.com).

For a full description of the module, visit the
[project page](https://www.drupal.org/project/botdetect).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/botdetect).

## Table of contents

- Requirements
- Installation
- Configuration
- Usage
- Maintainers

## Requirements

This module requires the following modules:

- [Captcha](https://www.drupal.org/project/captcha)

This module requires the following librairies:

- [jquery-captcha](https://github.com/captcha-com/jquery-captcha)

This module requires the following dependencies:

- [symfony-captcha-bundle](
  https://captcha.com/doc/php/symfony-captcha-bundle.html)

## Installation

### Module

Install as you would normally install a contributed Drupal module. For further
information, see [Installing Drupal Modules](
https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

### Libraries

Download the [jquery-captcha](https://github.com/captcha-com/jquery-captcha)
library and extract it to `/libraries` directory,
correct path for the javascript file should be
`/libraries/jquery-captcha/dist/jquery-captcha.min.js`.<br>

The `jquery-captcha` library can also be automatically installed if your
Drupal `composer` configuration handles `composer.libraries.json` files.

### Dependencies

The `symfony-captcha-bundle` library can be installed using `composer` by
running the following command:
`composer require captcha-com/symfony-captcha-bundle:"4.*"`

More information available on BotDetect web site:
https://captcha.com/doc/php/symfony-captcha-bundle.html

## Configuration

BotDetect captcha comes with a default configuration out of the box.

But you can upload a custom `botdetect.xml` file and specify the corresponding
captcha style name in the module configuration page.

## Usage

- To activate the botdetect challenge type on a CAPTCHA point, go
  to Configuration > People > CAPTCHA settings > Form settings
  (`/admin/config/people/captcha/captcha-points`), edit the CAPTCHA point of
  your choosing and set the "Challenge type" to "BotDetect".

- Alternatively, you can also set the BotDetect challenge type as your "Default
  challenge type" under Configuration > People > CAPTCHA Settings
  (`/admin/config/people/captcha`).

## Maintainers

- Frank Mably - [mably](https://www.drupal.org/u/mably)

Supporting organizations:

- [Bordeaux Metropole](https://www.bordeaux-metropole.fr)
