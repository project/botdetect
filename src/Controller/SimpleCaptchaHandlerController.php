<?php

namespace Drupal\botdetect\Controller;

use Captcha\Bundle\CaptchaBundle\Helpers\BotDetectSimpleCaptchaHelper;
use Drupal\botdetect\Support\DrupalSimpleLibraryLoader;
use Drupal\botdetect\Support\Path;
use Drupal\Core\Controller\ControllerBase;
use JetBrains\PhpStorm\NoReturn;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Handles all requests from the BotDetect Captcha widget.
 */
class SimpleCaptchaHandlerController extends ControllerBase {

  /**
   * Instance of a BotDetect SimpleCaptcha object.
   *
   * @var object
   */
  private $captcha;

  /**
   * Handle request from querystring such as getting image, getting sound, etc.
   */
  #[NoReturn] public function index(): void {
    $this->captcha = $this->getBotDetectCaptchaInstance();

    $commandString = $this->getUrlParameter('get');
    if (!\BDC_StringHelper::HasValue($commandString)) {
      \BDC_HttpHelper::BadRequest('command');
    }

    $commandString = \BDC_StringHelper::Normalize($commandString);
    $command = \BDC_SimpleCaptchaHttpCommand::FromQuerystring($commandString);
    $responseBody = '';
    switch ($command) {
      case \BDC_SimpleCaptchaHttpCommand::GetImage:
        $responseBody = $this->getImage();
        break;

      case \BDC_SimpleCaptchaHttpCommand::GetBase64ImageString:
        $responseBody = $this->getBase64ImageString();
        break;

      case \BDC_SimpleCaptchaHttpCommand::GetSound:
        $responseBody = $this->getSound();
        break;

      case \BDC_SimpleCaptchaHttpCommand::GetHtml:
        $responseBody = $this->getHtml();
        break;

      case \BDC_SimpleCaptchaHttpCommand::GetValidationResult:
        $responseBody = $this->getValidationResult();
        break;

      // Sound icon.
      case \BDC_SimpleCaptchaHttpCommand::GetSoundIcon:
        $responseBody = $this->getSoundIcon();
        break;

      case \BDC_SimpleCaptchaHttpCommand::GetSoundSmallIcon:
        $responseBody = $this->getSmallSoundIcon();
        break;

      case \BDC_SimpleCaptchaHttpCommand::GetSoundDisabledIcon:
        $responseBody = $this->getDisabledSoundIcon();
        break;

      case \BDC_SimpleCaptchaHttpCommand::GetSoundSmallDisabledIcon:
        $responseBody = $this->getSmallDisabledSoundIcon();
        break;

      // Reload icon.
      case \BDC_SimpleCaptchaHttpCommand::GetReloadIcon:
        $responseBody = $this->getReloadIcon();
        break;

      case \BDC_SimpleCaptchaHttpCommand::GetReloadSmallIcon:
        $responseBody = $this->getSmallReloadIcon();
        break;

      case \BDC_SimpleCaptchaHttpCommand::GetReloadDisabledIcon:
        $responseBody = $this->getDisabledReloadIcon();
        break;

      case \BDC_SimpleCaptchaHttpCommand::GetReloadSmallDisabledIcon:
        $responseBody = $this->getSmallDisabledReloadIcon();
        break;

      // css, js.
      case \BDC_SimpleCaptchaHttpCommand::GetScriptInclude:
        $responseBody = $this->getScriptInclude();
        break;

      case \BDC_SimpleCaptchaHttpCommand::GetLayoutStyleSheet:
        $responseBody = $this->getLayoutStyleSheet();
        break;

      case \BDC_SimpleCaptchaHttpCommand::GetP:
        $responseBody = $this->getP();
        break;

      default:
        \BDC_HttpHelper::BadRequest('command');
        break;
    }

    // Disallow audio file search engine indexing.
    header('X-Robots-Tag: noindex, nofollow, noarchive, nosnippet');
    echo $responseBody;
    exit;
  }

  /**
   * Get CAPTCHA object instance.
   *
   * @return object|null
   *   The SimpleCaptcha instance.
   */
  private function getBotDetectCaptchaInstance(): object|null {
    // Load BotDetect Library.
    $libraryLoader = new DrupalSimpleLibraryLoader();
    $libraryLoader->load();

    $captchaStyleName = $this->getUrlParameter('c');
    if (is_null($captchaStyleName) || !preg_match('/^(\w+)$/ui', $captchaStyleName)) {
      return NULL;
    }

    $captchaId = $this->getUrlParameter('t');
    if ($captchaId !== NULL) {
      $captchaId = \BDC_StringHelper::Normalize($captchaId);
      if (1 !== preg_match(\BDC_SimpleCaptchaBase::VALID_CAPTCHA_ID, $captchaId)) {
        return NULL;
      }
    }

    return new BotDetectSimpleCaptchaHelper($captchaStyleName, $captchaId);
  }

  /**
   * Get an input parameter from URL.
   *
   * @param string $param
   *   The parameter to get from input.
   *
   * @return string|null
   *   The value of the input parameter.
   */
  private function getUrlParameter($param): ?string {
    return filter_input(INPUT_GET, $param);
  }

  /**
   * Generate a Captcha image.
   *
   * @return image
   *   The generated BotDetect Captcha image.
   */
  public function getImage() {
    header("Access-Control-Allow-Origin: *");

    // Authenticate client-side request.
    $corsAuth = new \CorsAuth();
    if (!$corsAuth->IsClientAllowed()) {
      \BDC_HttpHelper::BadRequest($corsAuth->GetFrontEnd() . " is not an allowed front-end");
      return NULL;
    }

    if (is_null($this->captcha)) {
      \BDC_HttpHelper::BadRequest('captcha');
    }

    // Identifier of the particular Captcha object instance.
    $captchaId = $this->getCaptchaId();
    if (is_null($captchaId)) {
      \BDC_HttpHelper::BadRequest('instance');
    }

    // Image generation invalidates sound cache, if any.
    $this->clearSoundData($this->captcha, $captchaId);

    // Response headers.
    \BDC_HttpHelper::DisallowCache();

    // Response MIME type & headers.
    $imageType = \ImageFormat::GetName($this->captcha->ImageFormat);
    $imageType = strtolower($imageType[0]);
    $mimeType = "image/" . $imageType;
    header("Content-Type: {$mimeType}");

    // We don't support content chunking, since image files
    // are regenerated randomly on each request.
    header('Accept-Ranges: none');

    // Image generation.
    $rawImage = $this->getImageData($this->captcha);

    $length = strlen($rawImage);
    header("Content-Length: {$length}");
    return $rawImage;
  }

  /**
   * Gets the Captcha ID.
   *
   * @return string
   *   The captcha ID.
   */
  private function getCaptchaId() {
    $captchaId = $this->getUrlParameter('t');

    if (!\BDC_StringHelper::HasValue($captchaId)) {
      return NULL;
    }

    // captchaId consists of 32 lowercase hexadecimal digits.
    if (strlen($captchaId) != 32) {
      return NULL;
    }

    if (1 !== preg_match(\BDC_SimpleCaptchaBase::VALID_CAPTCHA_ID, $captchaId)) {
      return NULL;
    }

    return $captchaId;
  }

  /**
   * Clears the sound data.
   */
  private function clearSoundData($p_Captcha, $p_CaptchaId) {
    $p_Captcha->get_CaptchaPersistence()
      ->GetPersistenceProvider()
      ->Remove("BDC_Cached_SoundData_" . $p_CaptchaId);
  }

  /**
   * Gets the image data.
   */
  private function getImageData($p_Captcha) {
    // Identifier of the particular Captcha object instance.
    $captchaId = $this->getCaptchaId();
    if (is_null($captchaId)) {
      \BDC_HttpHelper::BadRequest('Captcha Id doesn\'t exist');
    }

    if ($this->isObviousBotRequest($p_Captcha)) {
      return;
    }

    // Image generation invalidates sound cache, if any.
    $this->clearSoundData($p_Captcha, $captchaId);

    // Response headers.
    \BDC_HttpHelper::DisallowCache();

    // We don't support content chunking, since image files
    // are regenerated randomly on each request.
    header('Accept-Ranges: none');

    // Disallow audio file search engine indexing.
    header('X-Robots-Tag: noindex, nofollow, noarchive, nosnippet');

    // Image generation.
    $rawImage = $p_Captcha->CaptchaBase->GetImage($captchaId);
    // Record generated Captcha code for validation.
    $p_Captcha->SaveCode($captchaId, $p_Captcha->CaptchaBase->Code);

    return $rawImage;
  }

  /**
   * Check if obvious bot request.
   */
  private function isObviousBotRequest($p_Captcha) {
    $captchaRequestValidator = new \SimpleCaptchaRequestValidator($p_Captcha->Configuration);

    // Some basic request checks.
    $captchaRequestValidator->RecordRequest();

    if ($captchaRequestValidator->IsObviousBotAttempt()) {
      \BDC_HttpHelper::TooManyRequests('IsObviousBotAttempt');
    }

    return FALSE;
  }

  /**
   * Get image in base64 format string.
   */
  public function getBase64ImageString() {
    header("Access-Control-Allow-Origin: *");

    // Authenticate client-side request.
    $corsAuth = new \CorsAuth();
    if (!$corsAuth->IsClientAllowed()) {
      \BDC_HttpHelper::BadRequest($corsAuth->GetFrontEnd() . " is not an allowed front-end");
      return NULL;
    }

    // MIME type.
    $imageType = \ImageFormat::GetName($this->captcha->ImageFormat);
    $imageType = strtolower($imageType[0]);
    $mimeType = "image/" . $imageType;

    $rawImage = $this->getImageData($this->captcha);

    $base64ImageString = sprintf('data:%s;base64,%s', $mimeType, base64_encode($rawImage));
    return $base64ImageString;
  }

  /**
   * Generate a Captcha sound.
   */
  public function getSound() {
    header("Access-Control-Allow-Origin: *");

    // Authenticate client-side request.
    $corsAuth = new \CorsAuth();
    if (!$corsAuth->IsClientAllowed()) {
      \BDC_HttpHelper::BadRequest($corsAuth->GetFrontEnd() . " is not an allowed front-end");
      return NULL;
    }

    if (is_null($this->captcha)) {
      \BDC_HttpHelper::BadRequest('captcha');
    }

    // Identifier of the particular Captcha object instance.
    $captchaId = $this->getCaptchaId();
    if (is_null($captchaId)) {
      \BDC_HttpHelper::BadRequest('Captcha Id doesn\'t exist');
    }

    if ($this->isObviousBotRequest($this->captcha)) {
      return;
    }

    $soundBytes = $this->getSoundData($this->captcha, $captchaId);

    if (is_null($soundBytes)) {
      \BDC_HttpHelper::BadRequest('Please reload the form page before requesting another Captcha sound');
      exit;
    }

    $totalSize = strlen($soundBytes);

    // Response headers.
    \BDC_HttpHelper::SmartDisallowCache();

    // Response MIME type & headers.
    $mimeType = $this->captcha->CaptchaBase->SoundMimeType;
    header("Content-Type: {$mimeType}");
    header('Content-Transfer-Encoding: binary');

    // Javascript player not used, we send the file directly as a download.
    if (!array_key_exists('d', $_GET)) {
      $downloadId = \BDC_CryptoHelper::GenerateGuid();
      header("Content-Disposition: attachment; filename=captcha_{$downloadId}.wav");
    }

    // iPhone/iPad sound issues workaround: chunked response for iOS clients.
    if ($this->detectIosRangeRequest()) {
      // Sound byte subset.
      $range = $this->getSoundByteRange();
      $rangeStart = $range['start'];
      $rangeEnd = $range['end'];
      $rangeSize = $rangeEnd - $rangeStart + 1;

      // Initial iOS 6.0.1 testing; leaving as fallback since we can't be sure
      // it won't happen again: we depend on observed behavior of invalid range
      // requests to detect end of sound playback, cleanup and tell
      // AppleCoreMedia to stop requesting invalid "bytes=rangeEnd-rangeEnd"
      // ranges in an infinite(?) loop.
      if ($rangeStart == $rangeEnd || $rangeEnd > $totalSize) {
        \BDC_HttpHelper::BadRequest('invalid byte range');
      }

      $rangeBytes = substr($soundBytes, $rangeStart, $rangeSize);

      // Partial content response with the requested byte range.
      header('HTTP/1.1 206 Partial Content');
      header('Accept-Ranges: bytes');
      header("Content-Length: {$rangeSize}");
      header("Content-Range: bytes {$rangeStart}-{$rangeEnd}/{$totalSize}");
      // Chrome needs this kind of response to be able to replay Html5 audio.
      return $rangeBytes;
    }
    elseif ($this->detectFakeRangeRequest()) {
      header('Accept-Ranges: bytes');
      header("Content-Length: {$totalSize}");
      $end = $totalSize - 1;
      header("Content-Range: bytes 0-{$end}/{$totalSize}");
      return $soundBytes;
      // Regular sound request.
    }
    else {
      header('Accept-Ranges: none');
      header("Content-Length: {$totalSize}");
      return $soundBytes;
    }
  }

  /**
   * Gets sound data.
   */
  public function getSoundData($p_Captcha, $p_CaptchaId) {
    $shouldCache = (
      // No sound regeneration allowed, so we must cache the first and only
      // generated sound.
      ($p_Captcha->SoundRegenerationMode == \SoundRegenerationMode::None) ||
      // Keep the same Captcha sound across all chunked iOS requests.
      $this->detectIosRangeRequest()
    );

    if ($shouldCache) {
      $loaded = $this->loadSoundData($p_Captcha, $p_CaptchaId);
      if (!is_null($loaded)) {
        return $loaded;
      }
    }
    else {
      $this->clearSoundData($p_Captcha, $p_CaptchaId);
    }

    $soundBytes = $this->generateSoundData($p_Captcha, $p_CaptchaId);
    if ($shouldCache) {
      $this->saveSoundData($p_Captcha, $p_CaptchaId, $soundBytes);
    }
    return $soundBytes;
  }

  // Instead of relying on unreliable user agent checks,
  // we detect the iOS sound.

  /**
   * Requests by the Http headers they will always contain.
   */
  private function detectIosRangeRequest() {
    if (array_key_exists('HTTP_RANGE', $_SERVER) &&
      \BDC_StringHelper::HasValue($_SERVER['HTTP_RANGE'])) {
      // Safari on MacOS and all browsers on <= iOS 10.x.
      if (array_key_exists('HTTP_X_PLAYBACK_SESSION_ID', $_SERVER) &&
        \BDC_StringHelper::HasValue($_SERVER['HTTP_X_PLAYBACK_SESSION_ID'])) {
        return TRUE;
      }

      $userAgent = array_key_exists('HTTP_USER_AGENT', $_SERVER) ? $_SERVER['HTTP_USER_AGENT'] : NULL;

      // All browsers on iOS 11.x and later.
      if (\BDC_StringHelper::HasValue($userAgent)) {
        $userAgentLC = \BDC_StringHelper::Lowercase($userAgent);
        if (\BDC_StringHelper::Contains($userAgentLC, "like mac os") || \BDC_StringHelper::Contains($userAgentLC, "like macos")) {
          return TRUE;
        }
      }
    }
    return FALSE;
  }

  /**
   * Loads sound data.
   */
  private function loadSoundData($p_Captcha, $p_CaptchaId) {
    $soundBytes = $p_Captcha->get_CaptchaPersistence()
      ->GetPersistenceProvider()
      ->Load("BDC_Cached_SoundData_" . $p_CaptchaId);
    return $soundBytes;
  }

  /**
   * Generates sound data.
   */
  private function generateSoundData($p_Captcha, $p_CaptchaId) {
    $rawSound = $p_Captcha->CaptchaBase->GetSound($p_CaptchaId);
    // Always record sound generation count.
    $p_Captcha->SaveCode($p_CaptchaId, $p_Captcha->CaptchaBase->Code);
    return $rawSound;
  }

  /**
   * Save sounds data.
   */
  private function saveSoundData($p_Captcha, $p_CaptchaId, $p_SoundBytes) {
    $p_Captcha->get_CaptchaPersistence()
      ->GetPersistenceProvider()
      ->Save("BDC_Cached_SoundData_" . $p_CaptchaId, $p_SoundBytes);
  }

  /**
   * Gets sound byte range.
   */
  private function getSoundByteRange() {
    // Chunked requests must include the desired byte range.
    $rangeStr = $_SERVER['HTTP_RANGE'];
    if (!\BDC_StringHelper::HasValue($rangeStr)) {
      return;
    }

    $matches = [];
    preg_match_all('/bytes=([0-9]+)-([0-9]+)/', $rangeStr, $matches);
    return [
      'start' => (int) $matches[1][0],
      'end' => (int) $matches[2][0],
    ];
  }

  /**
   * Detects fake range request.
   */
  private function detectFakeRangeRequest() {
    $detected = FALSE;
    if (array_key_exists('HTTP_RANGE', $_SERVER)) {
      $rangeStr = $_SERVER['HTTP_RANGE'];
      if (\BDC_StringHelper::HasValue($rangeStr) &&
        preg_match('/bytes=0-$/', $rangeStr)) {
        $detected = TRUE;
      }
    }
    return $detected;
  }

  /**
   * Returns captcha widget HTML.
   */
  public function getHtml() {
    header("Access-Control-Allow-Origin: *");

    $corsAuth = new \CorsAuth();
    if (!$corsAuth->IsClientAllowed()) {
      \BDC_HttpHelper::BadRequest($corsAuth->GetFrontEnd() . " is not an allowed front-end");
    }

    $html = "<div>" . $this->captcha->Html() . "</div>";
    return $html;
  }

  /**
   * The client requests the Captcha validation result.
   *
   * Used for Ajax Captcha validation.
   *
   * @return json
   *   The JSON validation result in JSON format.
   */
  public function getValidationResult() {
    header("Access-Control-Allow-Origin: *");

    // Authenticate client-side request.
    $corsAuth = new \CorsAuth();
    if (!$corsAuth->IsClientAllowed()) {
      \BDC_HttpHelper::BadRequest($corsAuth->GetFrontEnd() . " is not an allowed front-end");
      return NULL;
    }

    if (is_null($this->captcha)) {
      \BDC_HttpHelper::BadRequest('captcha');
    }

    // Identifier of the particular Captcha object instance.
    $captchaId = $this->getCaptchaId();
    if (is_null($captchaId)) {
      \BDC_HttpHelper::BadRequest('instance');
    }

    $mimeType = 'application/json';
    header("Content-Type: {$mimeType}");

    // Code to validate.
    $userInput = $this->getUserInput();

    // JSON-encoded validation result.
    $result = FALSE;
    if (isset($userInput) && (isset($captchaId))) {
      $result = $this->captcha->AjaxValidate($userInput, $captchaId);
    }
    $resultJson = $this->getJsonValidationResult($result);

    return $resultJson;
  }

  /**
   * Extract the user input Captcha code from the Ajax validation request.
   *
   * @return string
   *   The extracted user input Captcha code.
   */
  private function getUserInput() {
    // BotDetect built-in Ajax Captcha validation.
    $input = $this->getUrlParameter('i');

    if (is_null($input)) {
      // jQuery validation support, the input key may be just about anything,
      // so we have to loop through fields and take the first unrecognized one.
      $recognized = ['get', 'c', 't', 'd'];
      foreach ($_GET as $key => $value) {
        if (!in_array($key, $recognized)) {
          $input = $value;
          break;
        }
      }
    }

    return $input;
  }

  /**
   * Encodes the Captcha validation result in a simple JSON wrapper.
   *
   * @return string
   *   The boolean value as a string.
   */
  private function getJsonValidationResult($result) {
    $resultStr = ($result ? 'true' : 'false');
    return $resultStr;
  }

  /**
   * Get Reload Icon group.
   */
  public function getSoundIcon() {
    $filePath = realpath(Path::getPublicDirPathInLibrary() . 'bdc-sound-icon.gif');
    return $this->getWebResource($filePath, 'image/gif');
  }

  /**
   * Gets web resource.
   */
  private function getWebResource($p_Resource, $p_MimeType, $hasEtag = TRUE) {
    header("Content-Type: $p_MimeType");
    if ($hasEtag) {
      \BDC_HttpHelper::AllowEtagCache($p_Resource);
    }

    return file_get_contents($p_Resource);
  }

  /**
   * Gets small sound icon.
   */
  public function getSmallSoundIcon() {
    $filePath = realpath(Path::getPublicDirPathInLibrary() . 'bdc-sound-small-icon.gif');
    return $this->getWebResource($filePath, 'image/gif');
  }

  /**
   * Gets disabled sound icon.
   */
  public function getDisabledSoundIcon() {
    $filePath = realpath(Path::getPublicDirPathInLibrary() . 'bdc-sound-disabled-icon.gif');
    return $this->getWebResource($filePath, 'image/gif');
  }

  /**
   * Gets small disabled sound icon.
   */
  public function getSmallDisabledSoundIcon() {
    $filePath = realpath(Path::getPublicDirPathInLibrary() . 'bdc-sound-small-disabled-icon.gif');
    return $this->getWebResource($filePath, 'image/gif');
  }

  /**
   * Get Reload Icon group.
   */
  public function getReloadIcon() {
    $filePath = realpath(Path::getPublicDirPathInLibrary() . 'bdc-reload-icon.gif');
    return $this->getWebResource($filePath, 'image/gif');
  }

  /**
   * Gets small reload icon.
   */
  public function getSmallReloadIcon() {
    $filePath = realpath(Path::getPublicDirPathInLibrary() . 'bdc-reload-small-icon.gif');
    return $this->getWebResource($filePath, 'image/gif');
  }

  /**
   * Gets disabled reload icon.
   */
  public function getDisabledReloadIcon() {
    $filePath = realpath(Path::getPublicDirPathInLibrary() . 'bdc-reload-disabled-icon.gif');
    return $this->getWebResource($filePath, 'image/gif');
  }

  /**
   * Gets small disabled reload icon.
   */
  public function getSmallDisabledReloadIcon() {
    $filePath = realpath(Path::getPublicDirPathInLibrary() . 'bdc-reload-small-disabled-icon.gif');
    return $this->getWebResource($filePath, 'image/gif');
  }

  /**
   * Gets script to include on widget page.
   */
  public function getScriptInclude() {
    header("Access-Control-Allow-Origin: *");

    // Saved data for the specified Captcha object in the application.
    if (is_null($this->captcha)) {
      \BDC_HttpHelper::BadRequest('captcha');
    }

    // Identifier of the particular Captcha object instance.
    $captchaId = $this->getCaptchaId();
    if (is_null($captchaId)) {
      \BDC_HttpHelper::BadRequest('instance');
    }

    // Response MIME type & headers.
    header('Content-Type: text/javascript');
    header('X-Robots-Tag: noindex, nofollow, noarchive, nosnippet');

    // 1. load BotDetect script
    $resourcePath = realpath(Path::getPublicDirPathInLibrary() . 'bdc-simple-api-script-include.js');

    if (!is_file($resourcePath)) {
      throw new BadRequestHttpException(sprintf('File "%s" could not be found.', $resourcePath));
    }

    $script = $this->getWebResource($resourcePath, 'text/javascript', FALSE);

    // 2. load BotDetect Init script
    $script .= \BDC_SimpleCaptchaScriptsHelper::GetInitScriptMarkup($this->captcha, $captchaId);

    // Add remote scripts if enabled.
    if ($this->captcha->RemoteScriptEnabled) {
      $script .= "\r\n";
      $script .= \BDC_SimpleCaptchaScriptsHelper::GetRemoteScript($this->captcha, $this->getClientSideFramework());
    }

    return $script;
  }

  /**
   * Gets the client side framework parameter.
   */
  private function getClientSideFramework() {
    $clientSide = $this->getUrlParameter('cs');
    if (\BDC_StringHelper::HasValue($clientSide)) {
      $clientSide = \BDC_StringHelper::Normalize($clientSide);
      return $clientSide;
    }
    return NULL;
  }

  /**
   * Serves the BotDetect layout CSS stylesheet.
   */
  public function getLayoutStyleSheet() {
    $filePath = realpath(Path::getPublicDirPathInLibrary() . 'bdc-layout-stylesheet.css');
    return $this->getWebResource($filePath, 'text/css');
  }

  /**
   * Serves P ;).
   */
  public function getP() {
    header("Access-Control-Allow-Origin: *");

    // Authenticate client-side request.
    $corsAuth = new \CorsAuth();
    if (!$corsAuth->IsClientAllowed()) {
      \BDC_HttpHelper::BadRequest($corsAuth->GetFrontEnd() . " is not an allowed front-end");
      return NULL;
    }

    if (is_null($this->captcha)) {
      \BDC_HttpHelper::BadRequest('captcha');
    }

    // Identifier of the particular Captcha object instance.
    $captchaId = $this->getCaptchaId();
    if (is_null($captchaId)) {
      \BDC_HttpHelper::BadRequest('instance');
    }

    // Create new one.
    $p = $this->captcha->GenPw($captchaId);
    $this->captcha->SavePw($this->captcha, $captchaId);

    // Response data.
    $response = "{\"sp\":\"{$p->GetSP()}\",\"hs\":\"{$p->GetHs()}\"}";

    // Response MIME type & headers.
    header('Content-Type: application/json');
    header('X-Robots-Tag: noindex, nofollow, noarchive, nosnippet');
    \BDC_HttpHelper::SmartDisallowCache();

    return $response;
  }

}
